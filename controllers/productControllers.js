const Product = require("../models/Product");
const auth = require("../auth");
const User = require("../models/User");


// Create product (Admin only)
module.exports.addProduct = (req, res) => {
    let newProduct = new Product({
        productName: req.body.productName,
        description: req.body.description,
        price: req.body.price,
        stocks: req.body.stocks,
        imgSrc: req.body.imgSrc
    });

    Product.find({ productName: req.body.productName }).then(result => {
        if (result.length > 0) {
            return res.send("Product already exist!");
        } else {
            const userData = auth.decode(req.headers.authorization);
            if (userData.isAdmin) {
                return newProduct.save()
                    .then(product => {
                        console.log(product);
                        res.send(true)
                    })
                    .catch(error => {
                        console.log(error);
                        res.send(false);
                    })
            } else {
                return res.status(401).send("You don't have access to this page!");
            }
        }
    })
}

// Retrieve all ACTIVE products (Admin, Registered Users, Guests)
module.exports.getActiveProducts = (req, res) => {
    return Product.find({ isActive: true }).then(result => res.send(result));
}

// Retrieve all products (Admin only)
module.exports.getAllProducts = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        return Product.find({}).then(result => res.send(result));
    } else {
        return res.status(401).send("You don't have access to this page!")
    }
}

// Retrieve a single product
module.exports.getProduct = (req, res) => {
        return Product.findById(req.params.productId).then(result => res.send(result));
}

// Update product information
module.exports.updateProduct = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        let updateProduct = {
            productName: req.body.productName,
            description: req.body.description,
            price: req.body.price,
            stocks: req.body.stocks
        };

        Product.findByIdAndUpdate(req.params.productId, updateProduct, { new: true })
            .then(result => {
                res.send(true);
            })
            .catch(error => {
                res.send(false);
            })
    } else {
        return res.status(401).send("This action is not permitted!")
    }
}

// Archive a product (Admin only)
module.exports.archiveProduct = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let updateIsActive = {
        isActive: req.body.isActive
    };
    if (userData.isAdmin) {
        return Product.findByIdAndUpdate(req.params.productId, updateIsActive)
            .then(result => {
                res.send(true);
            })
            .catch(error => {
                res.send(false);
            })
    } else {
        return res.status(401).send("You don't have access to this page!");
    }
}
