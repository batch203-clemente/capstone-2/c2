const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { request } = require("express");


// Check email exists
module.exports.checkEmailExists = (req, res) => {
    return (User.find({ email: req.body.email })).then(result => {
        console.log(result);
        if (result.length > 0) {
            return res.send(true);
        } else {
            return res.send(false);
        }
    })
        .catch(error => res.send(error))
}

// Check mobile checkEmailExists
module.exports.checkMobileExists = (req, res) => {
  return (User.find({mobileNumber: req.body.mobileNumber})).then(result => {
      console.log(result);
      if (result.length > 0) {
          return res.send(true);
      } else {
          return res.send(false);
      }
  })
      .catch(error => res.send(error))
}

// User Registration
module.exports.registerUser = (req, res) => {
    User.find({ email: req.body.email }).then(result => {
        if (result.length > 0) {
            return res.send("Email already used");
        } else {
            User.find({ mobileNumber: req.body.mobileNumber }).then(result => {
                if (result.length > 0) {
                    return res.send("Mobile number already used");
                } else {
                    let newUser = new User({
                        firstName: req.body.firstName,
                        lastName: req.body.lastName,
                        email: req.body.email,
                        password: bcrypt.hashSync(req.body.password, 10),
                        mobileNumber: req.body.mobileNumber
                    });
                    console.log(newUser);
                    return newUser.save()
                        .then(user => {
                            console.log(user);
                            res.send(true);
                        })
                        .catch(error => {
                            console.log(error);
                            res.send(false);
                        })
                }
            })
        }
    })
}


// User Authentication
module.exports.loginUser = (req, res) => {
    return User.findOne({ email: req.body.email })
        .then(result => {
            if (result == null) {
                return res.send({ message: "User does not exist" });
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(result) })
                } else {
                    return res.send({ message: "Incorrect password!" });
                }
            }
        })
}


// Create Order
module.exports.createOrder = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        return res.status(401).send("This action is not permitted!");
    } else {
        let isUserUpdated = await User.findById(userData.id)
            .then(user => {
                user.orders.push({
                    totalAmount: req.body.totalAmount,
                    products: req.body.products
                })

                return user.save()
                    .then(result => {
                        console.log(result);
                        return true;
                    })
                    .catch(error => {
                        console.log(error);
                        return false;
                    })
            })
        console.log(isUserUpdated);

        let isProductUpdated;
        for (let i = 0; i < req.body.products.length; i++) {
            let data = {
                userId: userData.id,
                userEmail: userData.email,
                productId: req.body.products[i].productId,
                quantity: req.body.products[i].quantity
            };

            isProductUpdated = await Product.findById(data.productId)
                .then(product => {
                    if (product.stocks > 0) {
                        product.orders.push({
                            userId: data.userId,
                            userEmail: data.userEmail,
                            quantity: data.quantity
                        })

                        product.stocks -= data.quantity;

                        return product.save()
                            .then(result => {
                                console.log(result);
                                return true;
                            })
                            .catch(error => {
                                console.log(error);
                                return false;
                            })
                    } else {
                        return res.status(401).send("No stock available!");
                    }
                })
            console.log(isProductUpdated);
        }
        (isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false);
    }

}


// Retrieve User Details
module.exports.getProfile = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    return User.findById(userData.id).then(result => {
        result.password = "***";
        return res.send(result);
    })
}


// Set user as admin (Admin only)
module.exports.setAdmin = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        let updateUser = {
            isAdmin: req.body.isAdmin
        };

        return User.findByIdAndUpdate(req.params.userId, updateUser)
        .then(result => {
            res.send(true);
        })
        .catch(error => {
            res.send(false);
        })
    } else {
        return res.status(401).send("This action is not permitted!");
    }
}


// Retrieve authenticated user's orders
module.exports.getAuthUsersOrders = (req, res) => {
    return User.findById(req.params.userId)
    .then(result => {
        return res.send(result.orders);
    });
}


// Retrieve ALL orders
module.exports.getAllOrders = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        return User.find({}).then(result => {
            return res.send(result);
        });
    } else {
        return res.status(401).send("This action is not permitted!");
    }
}
