const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    stocks: {
        type: Number,
        required: [true, "Stocks is required"]
    },
    imgSrc: {
        type: String
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [
        {
            orderId: {
                type: String
            },
            userId: {
                type: String,
                required: [true, "User ID is required"]
            },
            userEmail: {
                type: String,
                required: [true, "Email is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required"]
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
});

module.exports = mongoose.model("Product", productSchema);
