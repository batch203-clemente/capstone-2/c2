const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cors = require("cors");
const port = process.env.PORT || 4000;

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.7dj5uhv.mongodb.net/ecom_api?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(port, () => console.log(`Application is running on port ${port}`));
