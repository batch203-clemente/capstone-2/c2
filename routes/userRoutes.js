const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// Check email exists
router.post("/checkEmail", userControllers.checkEmailExists);
// Check mobile exists
router.post("/checkMobile", userControllers.checkMobileExists);
// Route for user registration
router.post("/register", userControllers.registerUser);
// Route for user authentication
router.post("/login", userControllers.loginUser);
// Route for creating an order
router.post("/checkout", auth.verify, userControllers.createOrder)
// Route for user details
router.get("/details", auth.verify, userControllers.getProfile);
// Route for setting user as admin
router.patch("/setUserAdmin/:userId", auth.verify, userControllers.setAdmin);
// Route for getting authenticated user's orders
router.get("/authUsersOrders/:userId", userControllers.getAuthUsersOrders);
// Route for getting ALL orders (Admin only)
router.get("/getAllOrders", auth.verify, userControllers.getAllOrders);


module.exports = router;
