const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating products (Admin only)
router.post("/", auth.verify, productControllers.addProduct);
// Route for retrieving all ACTIVE products
router.get("/allActiveProducts", productControllers.getActiveProducts);
// Route for retrieving ALL products
router.get("/allProducts", auth.verify, productControllers.getAllProducts);
// Route for retrieving a single product
router.get("/getProduct/:productId", productControllers.getProduct);
// Route for updating a product (Admin only)
router.put("/updateProduct/:productId", auth.verify, productControllers.updateProduct);
// Route for archiving a product (Admin only)
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);


module.exports = router;
